To launch vivado on a TP campux machine, open a terminal and run the following commands:

```bash
SETUP MEE_VIVADO_CLASSROOM # sets up the environment (license, PATH)

vivado
```

```asciinema-player
{
    "file": "cast/launch-vivado.cast",
    "mkap_theme": "penguin",
    "auto_play": false,
    "cols": 100,
    "rows": 36,
    "control": "auto",
    "idleTimeLimit": 1,
    "speed": 1.5
}
```

The launch of the tool can be quite long.
