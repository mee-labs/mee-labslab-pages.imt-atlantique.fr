# Choice, design and prototyping of a sound processing algorithm

## Objectives

Now that you have taken control of the design environment, the prototyping board and digital signals in VHDL, you must propose a sound processing algorithm that you will prototype.

Choose a relatively simple algorithm, such as:

- a more elaborate filter such as an IIR or a comb filter?
- a resonator (programmable?)
- reverb or any effect that tempts you?
- a couple compression/decompression like DCT?

Think about defining simple validation steps, measurement points that you can make on the board to allow debugging (what audio signal could you listen to)?

Then, you will propose, describe and test a hardware architecture to integrate this fixed-point algorithm, knowing that the audio samples are available in stereo and coded on 24 bits on the available board.

## Phases to respect in the design of a digital hardware architecture

- **The first step** is to have a fixed-point reference code of your algorithm and a set of test vectors. You can start from a Python, Julia, Matlab, C or C++ script, whatever. It is just necessary that your signals are represented by integers.
- **The second step** is to propose an architectural breakdown of your hardware implementation. We advise you to clearly distinguish the control, communication and processing units (operative unit) and to define the constituent sub-modules.
- **The third step** consists of the VHDL description phase. Do not rush into it too quickly! The architecture must be validated beforehand!

## Validation

It is likely that your algorithm will not work on the board immediately! Therefore, design the architecture in an agile way by allowing useful checkpoints for signal processing debugging. Partial validations must be planned!
