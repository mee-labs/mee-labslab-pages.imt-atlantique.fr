# FPGA Boards documentation

## Digilent Nexys 4

- Main page: [https://digilent.com/reference/programmable-logic/nexys-4/start](https://digilent.com/reference/programmable-logic/nexys-4/start){target="_blank"}
- Reference manual web page: [https://digilent.com/reference/programmable-logic/nexys-4/start](https://digilent.com/reference/programmable-logic/nexys-4/start){target="_blank"}
- Reference manual, PDF: [https://digilent.com/reference/_media/reference/programmable-logic/nexys-4/nexys4_rm.pdf](https://digilent.com/reference/_media/reference/programmable-logic/nexys-4/nexys4_rm.pdf){target="_blank"}
- XDC reference constraints files: [https://github.com/Digilent/digilent-xdc/blob/master/Nexys-4-Master.xdc](https://github.com/Digilent/digilent-xdc/blob/master/Nexys-4-Master.xdc){target="_blank"}

## Digilent Nexys 4 DDR

- Main page: [https://digilent.com/reference/programmable-logic/nexys-4-ddr/start](https://digilent.com/reference/programmable-logic/nexys-4-ddr/start){target="_blank"}
- Reference manual web page: [https://digilent.com/reference/programmable-logic/nexys-4-ddr/reference-manual](https://digilent.com/reference/programmable-logic/nexys-4-ddr/reference-manual){target="_blank"}
- Reference manual, PDF: [https://digilent.com/reference/_media/reference/programmable-logic/nexys-4-ddr/nexys4ddr_rm.pdf](https://digilent.com/reference/_media/reference/programmable-logic/nexys-4-ddr/nexys4ddr_rm.pdf){target="_blank"}
- XDC reference constraints files: [https://github.com/Digilent/digilent-xdc/blob/master/Nexys-4-DDR-Master.xdc](https://github.com/Digilent/digilent-xdc/blob/master/Nexys-4-DDR-Master.xdc){target="_blank"}

## Digilent Nexys A7

- Main page: [https://digilent.com/reference/programmable-logic/nexys-a7/start](https://digilent.com/reference/programmable-logic/nexys-a7/start){target="_blank"}
- Reference manual web page: [https://digilent.com/reference/programmable-logic/nexys-a7/reference-manual](https://digilent.com/reference/programmable-logic/nexys-a7/reference-manual){target="_blank"}
- XDC reference constraints files: [https://github.com/Digilent/digilent-xdc/blob/master/Nexys-A7-100T-Master.xdc](https://github.com/Digilent/digilent-xdc/blob/master/Nexys-A7-100T-Master.xdc){target="_blank"}
