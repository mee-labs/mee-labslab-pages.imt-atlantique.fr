# UE connected medical devices: Mini-project of digital integration of signal processing algorithms

## VHDL Cheat Sheet

[VHDL Cheat Sheet](../memo/index.md)

## Group and associated Gitlab repositories

A Gitlab group, grouping together all the different git repositories, has been created on [https://gitlab-df.imt-atlantique.fr](https://gitlab-df.imt-atlantique.fr) for each student. In this group we find the repositories corresponding to the projects:

- Loto
- Sound processor, audio filtering
- EGC filtering

To manipulate these repositories locally, it happens on the command line via the `git` commands: `clone`, `add`, `commit`, `push`, `pull`. Please refer to the page [Git and Gitlab](../gitlab.md).

## Objective and rules to follow

This Practical Work session is an introduction to the design of digital integrated circuits. It allows you to discover the different stages of the description of an architecture in VHDL (a language dedicated to electronics) up to testing on a reconfigurable FPGA (Field-Programmable Gate Array) circuit, all with a professional tool, Vivado from the circuit manufacturer AMD (which acquired Xilinx for its FPGA technologies in 2022). The main stages of the design of a circuit on an FPGA are as follows:

1. **design of the architecture** of the circuit in the form of blocks/modules organized hierarchically
2. **description of the modules** which can be done through hardware description languages (VHDL or Verilog among others) or through CAD tools offering pre-designed parametric modules
3. **functional simulation** of each module
4. **integration** of the modules to realize the complete circuit
5. **simulation** of the complete circuit
6. **synthesis** of the circuit transforming the hierarchical description into a list of logical components (elementary gates connected)
7. **post-synthesis simulation** to verify that the synthesized model is consistent with the initial model
8. **physical implementation** which consists in translating the previous list into a list of components of the target technology (a given FPGA for example), then placing them on the target hardware and routing them
9. **post-placement-routing simulation** to verify the performance compliance
10. **configuration and testing of the circuit**

Each of the simulation and test stages can trigger *a more or less significant rollback in case of failure*.

In practice, you will follow a simplified flow, benefiting from the preparatory work of the teachers:

**Phase 1** Description and simulation of simple combinational and sequential modules:

- a "fill-in-the-blanks" VHDL description is provided and must be completed based on the description provided
- each VHDL module is then simulated and corrected if necessary

**Phase 2** Synthesis, placement, routing and testing of the complete circuit:

- the hardware "compilation" flow must be parameterized and then launched
- the result of this flow is analyzed and then tested on the board

We ask you to respect the fundamental rules which are:

- **R1** The use of CAD tools should only start when the circuit architecture is finalized, i.e. divided into elementary modules (counter, register, decoder, etc.) and the interconnections between these functions are perfectly defined.
- **R2** The circuit is then fully described in VHDL (for this TP)
- **R3** The circuit is gradually validated by unit simulations
- **R4** The circuit is fully **synchronous**. This implies that:
    - **R4.1** all flip-flops are controlled by the same clock CLK
    - **R4.2** the copy of the input to the output of the flip-flop is done on the rising edge of the clock
    - **R4.3** the asynchronous input to reset to 0 is only used at the initialization of the circuit and is active at a level that will be the same throughout the circuit, it should not be used during normal operation of the circuit

!!! warning
    Before tackling this TP, it is therefore necessary to review the main concepts of digital electronics, in particular the sequential functions which must be entirely synchronous in this TP. Otherwise, no miracle...

## TP 1 Introduction to digital circuit design on FPGA: Loto

See the Loto TP subject here: [Loto Lab](loto.md)

## TP 2 Design of a sound processor

See the subject of the sound processor audio filter TP here: [Filter Lab](filter.md)

## TP 3 ECG signal processing

See the ECG filter TP subject here: [ECG Lab](ecg.md)
