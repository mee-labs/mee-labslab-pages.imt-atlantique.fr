# Constitution of a VHDL file

## Comments and headers

Comments are essential to the proper understanding of a code. The delimiter of comments is `--` (two classic dashes in a row). To comment several lines, you must place `--` on each line. It is possible to possible to place a comment at the end of a line of code. Example:


```.vhdl
-- first line of a comment in VHDL
-- second line of a comment in VHDL
signal U : unsigned(7 downto 0); -- inline comment in VHDL
```

It is also important in the header of a file to give information about the author of the code, the version, date of creation/modification etc.



## Libraries
The libraries contain mainly the definitions of the types and the arithmetic operators associated with them. The libraries to be preferred are the standard IEEE libraries. Declaration:

```.vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
```

## Entity

In the entity part, we describe the inputs and outputs of the component. We can also, via the `generic` define configuration parameters of the component. For example the size of some internal signals or inputs and outputs. Example of an entity with generics:

```.vhdl
entity EntityExample is
  generic(
    G_Size1 : integer := 4;
    G_Size2 : integer := 6
  );
  port(
    I_Input1   : in  std_logic;
    I_Input2   : in  std_logic;
    I_Input3   : in  std_logic_vector(G_Size1 -1 downto 0);
    I_Input4   : in  std_logic_vector(G_Size2 -1 downto 0);
    O_Outpout1 : out std_logic_vector(G_Size1 -1 downto 0);
    O_Outpout2 : out std_logic_vector(G_Size2 -1 downto 0)
  );
end entity EntityExample;
```

The entity is used to describe the interfaces of a component:

<figure markdown>
  ![Entity](../img/memo/Entity.png)
  <figcaption>Entity</figcaption>
</figure>


## Architecture

The architecture part is dedicated to the description of the component itself, i.e. its functioning, its constitution.

```.vhdl
architecture archi_EntityExample of EntityExample is

    -- type/signal/constant/component declaration

begin

    -- componant structure / behavior / process

end; -- end of file
```


##  Example of a complete file

```.vhdl
------------------------------------------------------------------
-- Title              : VHDLExample
-- Project            : Project example
-- creation           : 2014-04-01
-- File               : VHDLExample.vhd
-- Author             : name @mail
-- Company            : Telecom Bretagne
-- Last update        : 2014-04-01
-- Platform           : constructor ; model (Xilinx, altera...)
-- brief Description  : This module does/performs/computes stuffs
------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity VHDLExample is
  generic(
    G_AddWidth : integer := 4;
    G_WordSize : integer := 6
  );
  port(
    I_sys_clk : in  std_logic;
    I_we      : in  std_logic;
    I_adr     : in  std_logic_vector(G_AddWidth -1 downto 0);
    I_di      : in  std_logic_vector(G_WordSize -1 downto 0);
    O_do      : out std_logic_vector(G_WordSize -1 downto 0)
  );
end VHDLExample;

architecture archi_VHDLExample of VHDLExample is

  type ram_type is array(2**G_AddWidth-1 downto 0)of std_logic_vector(G_WordSize-1 downto 0);
  signal RAM   :     ram_type;
  signal SR_do : out std_logic_vector(G_WordSize -1 downto 0);

begin

  process (I_sys_clk)
  begin
    if(rising_edge(I_sys_clk)then
      if(I_we = '1')then
        RAM(to_unsigned(I_adr)) <= I_di;
      end if;
      -- synchronous output RAM : uses block RAM in FPGA instead of LUT :
      SR_do <= RAM(to_unsigned(I_adr));
    end if;
  end process;

  O_do <= SR_do;

end archi_VHDLExample;
```
